### **X11**
Las *X11* se encargan de dibujar en nuestra pantalla lo que nosotros queremos ver. 

Las **X11** dibujan por pantalla, pero la **CPU,RAM y Disco Duro** son los encargados de ejecutarlas.

### **Conexión SSH**
Esta conexión nos sirve para conectarnos al equipo de otra persona, o incluso al de una empresa.

Para poder establecer esta conexión,necesitaremos las **X11 fordwaring**, las cuales usamos para indicar quién tendrá el servidor en el equipo.

Para establecer la conexión, seguiremos estos pasos:

- **systemctl start sshd** (*para poner en marcha el servicio ssh*)

- **systemctl status sshd** (*para comprobar el estado del ssh*)

- **ssh -X root@192.168.4.4** (*para conectarnos a la máquina que queremos ponemos: **ssh -X usuario@dirección IP** de la otra máquina*) 

- **contraseña de root** 

- lanzamos el programa **gparted** para ver el disco del otro equipo.


 
Para que funcione correctamente la conexión y nos muestre lo que queremos ver, el equipo que ejecute el comando tiene que tener instaladas las **X11**. 

**Con que el equipo que lance el comando las tenga**, ya podrá mostrarlo por nuestra pantalla.

### **Ventajas**  
Una ventaja de esta conexión es que podremos configurar un servidor o cualquier equipo **gráficamente** desde otra máquina, incluido nuestro portátil u ordenador.